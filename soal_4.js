// berikute soal untuk total praktek!
// gunakan deno!

// buatlah 2 file
// file pertama bernama "lib.js"
// export sebuah function dengan nama
// "print". function "print" menerima sebuah parameter
// dimana function tersebut akan mem-nulis teks pada parameternya

// export sebuah class, dengan spesifikasi
// nama class Cat
// property
// - name : String
// - color : String
// - sounds : String
// methodnya
// - describe
// - wantEat

// penjelasan method
// - describe -> memprint (gunakan function print) teks dengan format
//   "kucing ini bernama <name> memiliki warna <color>". Gunakan backtip
// - wantEat -> looping 3x dan print property sound 3x menggunakan function print

// file kedua bernama "app.js"
// import file print dan class Cat dari file lib.js
// pertama print teks "evaluasi javascript"
// lalu buat sebuah variable yang memiliki value object Cat
// lalu jalankan method eat dan describe
