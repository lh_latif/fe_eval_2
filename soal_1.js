// object
// buat sebuah variable yang
// valuenya object
// dengan spesifikasi sebagai berikut:
// property
// - name : String
// - description : String
// - isUsed : Boolean
// - price : Integer
// - quantity : Integer
// - wantToSell : Boolean
// method
// - sell
// - cancel
// 
// penjelasan method
// - sell -> method sell akan menampilkan teks jika wantToSell bernilai true,
//   dengan format: "dijual <name> sejumlah <quantity> dengan harga <price>".
//   name, quantity, dan price diambil dari value property object
//
// - cancel -> method cancel akan mengisi ulang value dari property wantToSell menjadi false
