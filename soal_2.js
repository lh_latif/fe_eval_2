// gunakan deno!
// module
// buatlah 2 file

// file pertama bernama "ads.js"
// lalu di dalamnya export sebuah class.
// class tersebut memiliki nama "Ads".
// property
// - name : String
// - description : String
// - isUsed : Boolean
// - price : Integer
// - quantity : Integer
// - wantToSell : Boolean
// method
// - sell
// - cancel
//
// penjelasan method
// - sell -> method sell akan menampilkan teks jika wantToSell bernilai true,
//   dengan format: "dijual <name> sejumlah <quantity> dengan harga <price>".
//   gunakan literal
//   name, quantity, dan price diambil dari value property object
//
// - cancel -> method cancel akan mengisi ulang value dari property wantToSell menjadi false


// file kedua bernama "app.js"
// import class Ads dari file ads.js
// buatlah 2 variable yang masing-masing memiliki value object Ads.
// buat object Ads menggunakan class Ads.
// pada salah satu variable panggil method "sell"
// lalu satu lainnya panggil method "cancel"
