// buat sebuah variable yang memiliki value high order function.
// high order function tersebut me-return promise.

// logika didalam high order function Promise
// adalah buatlah variable "angka" yang valuenya didapat dari randomizer
// jika angka lebih kecil dari 3 maka resolve
// jika sebaliknya maka reject

// lalu panggil variable yang memiliki high order function tersebut

// buatlah 2 function

// function pertama memanggil variable hof dan kendalikan Promise-
// menggunakan object

// function kedua memanggil variable hof dan kendalikan Promise menggunakan async-await

function randomizer() {
  return (Math.random() * 10);
}
